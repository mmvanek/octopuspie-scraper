#!/usr/bin/env python3
'''
crawl
    Crawls webcomic octopuspie for the URL to each of its comics and outputs
    results to stdout
'''

from contextlib import suppress
import re
from os.path import abspath, dirname, join
from random import random
from time import sleep
from urllib.request import urlopen


START_URL = 'http://www.octopuspie.com/2008-01-09/094-cushion/'
BASE_PATH = dirname(abspath(__file__))
LAST_URL_FNAME = join(BASE_PATH, 'data/last-url.txt')
IMG_LIST_FNAME = join(BASE_PATH, 'data/img-list.txt')


class NoUpdatesException(Exception):
    pass


def find_next(html):
    p = re.compile('<a href="[^"]+" rel="next"')
    try:
        m = p.findall(html)[0]
    except IndexError:
        return False
    return m[9:-12]


def find_img(html):
    p = re.compile('http://www.octopuspie.com/strippy/\d\d\d\d-\d\d-\d\d\.[^"]+')
    return p.findall(html)[0]


def crawl_page():
    try:
        with open(LAST_URL_FNAME, 'r') as f:
            cur_url = f.read().strip()
    except FileNotFoundError:
        cur_url = START_URL
    else:
        with urlopen(cur_url) as s:
            res = s.read()
        cur_url = find_next(res.decode('utf-8'))
        if not cur_url:
            raise NoUpdatesException
    while cur_url:
        print(cur_url)
        with urlopen(cur_url) as s:
            res = s.read()
        html = res.decode('utf-8')
        with open(LAST_URL_FNAME, 'w') as f:
            print(cur_url, file=f)
        with open(IMG_LIST_FNAME, 'a') as f:
            print(find_img(html), file=f)
        cur_url = find_next(html)
        sleep(random()*3)


def main():
    with suppress(NoUpdatesException):
        crawl_page()
    return 0

if __name__ == '__main__':
    exit(main())
