#!/usr/bin/env python3

from crawl import crawl_page, NoUpdatesException, IMG_LIST_FNAME
from download import download_list


def main():
    print('Checking for new strips...')
    try:
        crawl_page()
    except NoUpdatesException:
        print('No updates')
        return 1
    print('Downloading new strips...')
    download_list(IMG_LIST_FNAME)
    return 0

if __name__ == '__main__':
    exit(main())
