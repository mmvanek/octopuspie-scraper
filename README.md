# README #

Run `octopuspie.py` to download all new strips. Run `crawl.py` if you want to only update the local list of image urls. Run `download.py` if you want to download any new files in the list to the `img/` directory without updating the local list of image urls.

### Requirements ###

* [Python] >=3.5
* [curl]

[curl]: <https://curl.haxx.se/>
[Python]: <https://www.python.org/>