#!/usr/bin/env python3
'''
download
    Downloads web files pointed to by a list of newline-separated URIs
'''

from os.path import abspath, basename, dirname, isfile, join
from random import random
import subprocess
from sys import argv
from time import sleep


BASE_PATH = dirname(abspath(__file__))
# Where to save the images
IMG_DIRNAME = join(BASE_PATH, 'img')


def usage():
    print('usage: python download.py <url-list>\n'
          '    <url-list>: newline-separated list of URLs to download sequentially')
    return 1


def download_url(url):
    img_fname = url.split('/')[-1]
    img_path = join(IMG_DIRNAME, img_fname)
    if isfile(img_path):
        raise FileExistsError
    print(url)
    with open(img_path, 'w') as f:
        subprocess.run(['curl', url.strip()], stdout=f)


def download_list(infile):
    with open(infile) as f:
        for url in f:
            url = url.strip()
            try:
                download_url(url)
            except FileExistsError:
                pass
            else:
                sleep(random()*3 + 1)


def main(infile):
    download_list(infile)
    return 0


if __name__ == '__main__':
    try:
        infile = argv[1]
    except IndexError:
        exit(usage())
    if not isfile(infile):
        exit(usage())
    exit(main(infile))
